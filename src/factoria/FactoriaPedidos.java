package factoria;


import dtos.Cliente;
import dtos.Pedido;
import dtos.Producto;

import java.util.ArrayList;
import java.util.List;

public class FactoriaPedidos {

    private static FactoriaPedidos factoria= new FactoriaPedidos();

    private FactoriaPedidos(){

    };

    public static FactoriaPedidos getFactoria(){
        return factoria;
    }

    public  List<Pedido> getPedidos(int num){

        List<Pedido> pedidos= new ArrayList<>();

        for ( int i=0; i<num; i++){
            pedidos.add(crearPedido(i));
        }

        return pedidos;

    }

    private  Pedido crearPedido(int i) {
        return new Pedido(crearProducto(i),crearCliente(i));
    }

    private Cliente crearCliente(int i){
        return new Cliente(i,"nombre_"+i,"apellidos_"+i,"67889993"+i,"example_"+i+"@email.com");
    }

    private Producto crearProducto(int i){
        return new Producto(i,"nombre_"+i,"marca"+i);
    }
}
