package service;

import dtos.Cliente;
import dtos.Pedido;
import util.GestorAnotaciones;

import java.util.List;

/**
 * Created by Isaac on 28/04/2016.
 */
public class GestorPedidos {

    public void mostrarPedidos(List<Pedido> pedidos){
        for (Pedido p: pedidos){
            System.out.print("Cliente: ");
            GestorAnotaciones.imprimirObjetoAnotado(p.getCliente());
            System.out.print(" ");
            System.out.print("Producto: ");
            GestorAnotaciones.imprimirObjetoAnotado(p.getProducto());
            System.out.println();
        }
    }

}
