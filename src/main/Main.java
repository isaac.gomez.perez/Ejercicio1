package main;

import dtos.Pedido;
import factoria.FactoriaPedidos;
import service.GestorPedidos;
import util.GestorAnotaciones;

import java.util.List;

/**
 * Created by Isaac on 21/04/2016.
 */
public class Main {

    public static void main(String[] args) {

        int numePedidos=10;
        List<Pedido> pedidos = FactoriaPedidos.getFactoria().getPedidos(numePedidos);
        GestorPedidos gestorPedidos = new GestorPedidos();

        gestorPedidos.mostrarPedidos(pedidos);

    }
}
