package dtos;

/**
 * Created by Isaac on 21/04/2016.
 */
public class Pedido {

    private Producto producto;
    private Cliente cliente;

    public Pedido(Producto producto, Cliente cliente) {
        this.producto = producto;
        this.cliente = cliente;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public String toString() {
        return cliente.toString()+", "+producto.toString();
    }
}
