package util;

import annotations.Cifrado;

import java.lang.reflect.Field;

/**
 * Created by Isaac on 21/04/2016.
 */
public class Cifrar {

    public static String cifradoCesar(String original, int clave) {

        StringBuilder cifrado = new StringBuilder(original.length());
        int valorASCII = 0;

        for (int i = 0; i < original.length(); i++) {

            valorASCII = (int) (original.charAt(i));
            valorASCII = valorASCII + clave % 255;
            cifrado.append((char) (valorASCII));
        }
        return cifrado.toString();
    }


}
