package util;

import annotations.Cifrado;
import annotations.Mayusculas;

import java.lang.reflect.Field;

/**
 * Created by Isaac on 21/04/2016.
 */
public class GestorAnotaciones {

    public static void imprimirObjetoAnotado(Object o){
        Class clase = null;
        try {
            clase =Class.forName(o.getClass().getCanonicalName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        for (Field f : clase.getDeclaredFields()){
            Object value = null;
            f.setAccessible(true);
            try {
                value = f.get(o);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            if (f.isAnnotationPresent(Mayusculas.class)){
                System.out.print(value.toString().toUpperCase() +", ");
            }else if (f.isAnnotationPresent(Cifrado.class)){
                System.out.print(Cifrar.cifradoCesar(value.toString(),f.getAnnotation(Cifrado.class).indice()) + ", ");
            }else{
                System.out.print(value.toString() +", ");
            }
        }
    }
}
