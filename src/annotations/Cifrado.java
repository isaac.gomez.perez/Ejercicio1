package annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Isaac on 21/04/2016.
 *
 * Cifrado Cesar coges el caracter ascii de cada palabra y le sumas el indice pasado por la anotacion y muestras su equivalente ascii
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Cifrado {
    int indice();
}
